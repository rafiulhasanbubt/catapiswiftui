//
//  BreedImageModel.swift
//  CatAPISwiftUI
//
//  Created by rafiul hasan on 30/9/21.
//

import Foundation

struct BreedImage: Codable {
    let height: Int?
    let id: String?
    let url: String?
    let width: Int?
}
