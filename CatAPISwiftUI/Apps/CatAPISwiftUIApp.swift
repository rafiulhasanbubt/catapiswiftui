//
//  CatAPISwiftUIApp.swift
//  CatAPISwiftUI
//
//  Created by rafiul hasan on 30/9/21.
//

import SwiftUI

@main
struct CatAPISwiftUIApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
