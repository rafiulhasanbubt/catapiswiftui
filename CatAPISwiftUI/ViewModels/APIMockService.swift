//
//  APIMockService.swift
//  CatAPISwiftUI
//
//  Created by rafiul hasan on 30/9/21.
//

import Foundation

struct APIMockService: APIServiceProtocol {
    
    var result: Result<[Breed], APIError>
    
    func fetchBreeds(url: URL?, completion: @escaping (Result<[Breed], APIError>) -> Void) {
        completion(result)
    }
}
