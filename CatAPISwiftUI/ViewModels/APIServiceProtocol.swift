//
//  APIServiceProtocol.swift
//  CatAPISwiftUI
//
//  Created by rafiul hasan on 30/9/21.
//

import Foundation

protocol APIServiceProtocol {
    func fetchBreeds(url: URL?, completion: @escaping(Result<[Breed], APIError>) -> Void)
}
